/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrhandler;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import rsrhandler.exports.CSV;
import rsrhandler.exports.Excel;
import rsrhandler.exports.Export;
import rsrhandler.exports.ExportConfig;
import utils.data.AEButtonColumns;
import utils.data.AEConfig;
import utils.data.AEData;
import utils.data.Basics;
import utils.data.beans.Disk;
import utils.data.actions.MyDiskAction;
import utils.data.actions.MyProgramAction;
import utils.data.MyTableModel;
import utils.data.actions.MyPartitionAction;
import utils.database.ReguladorConexion;

/**
 *
 * @author eryalus
 */
public class AE extends javax.swing.JFrame {

    private ArrayList<AEData> data;
    private final ReguladorConexion reg;

    private String addCeros(String txt, int tamaño) {
        int toadd = tamaño - txt.length();
        if (toadd > 0) {
            String temp = "";
            for (int i = 0; i < toadd; i++) {
                temp += "0";
            }
            temp += txt;
            return temp;
        } else {
            return txt;
        }
    }

    /**
     * Creates new form Programas
     *
     * @param data
     * @param config
     * @param reg
     */
    public AE(ArrayList<AEData> data, AEConfig config, ReguladorConexion reg) {
        initComponents();
        super.setTitle("A/E");
        this.reg = reg;
        this.data = data;
        ArrayList<String> names = new ArrayList<>();
        int size = 2;
        names.add("Aula");
        names.add("Equipo");
        if (config.isProcessor()) {
            names.add("Procesador");
            size++;
        }
        if (config.isCores()) {
            names.add("Núcleos");
            size++;
        }
        if (config.isMother()) {
            names.add("Placa");
            size++;
        }
        if (config.isVideo()) {
            names.add("Video");
            size++;
        }
        if (config.isRam()) {
            names.add("RAM");
            size++;
        }
        if (config.isMac()) {
            names.add("MAC");
            size++;
        }
        if (config.isIP()) {
            names.add("IP");
            size++;
        }
        if (config.isDate()) {
            names.add("Fecha");
            size++;
        }
        if (config.isDisks()) {
            names.add("Espacio");
            names.add("Discos");
            size += 2;
        }
        if (config.isPrograms()) {
            names.add("Programas");
            size++;
        }
        if (config.isPartitions()) {
            names.add("Particiones");
            size++;
        }
        String[] columnnames = new String[names.size()];
        for (int i = 0; i < names.size(); i++) {

            columnnames[i] = names.get(i);
        }

        MyTableModel dtm = new MyTableModel(columnnames);
        while (dtm.getRowCount() > 0) {
            dtm.removeRow(0);
        }
        for (int i = 0; i < data.size(); i++) {
            Object ob[] = new Object[size];
            AEData dat = data.get(i);
            ob[0] = dat.getAula();
            ob[1] = addCeros(dat.getEquipo(), 2);
            int posicion = 2;
            if (config.isProcessor()) {
                ob[posicion] = dat.getProcesador();
                posicion++;
            }
            if (config.isCores()) {
                ob[posicion] = dat.getCores();
                posicion++;
            }
            if (config.isMother()) {
                ob[posicion] = dat.getMother();
                posicion++;
            }
            if (config.isVideo()) {
                ob[posicion] = dat.getVideo();
                posicion++;
            }
            if (config.isRam()) {
                ob[posicion] = dat.getRam();
                posicion++;
            }
            if (config.isMac()) {
                ob[posicion] = dat.getMAC();
                posicion++;
            }
            if (config.isIP()) {
                ob[posicion++] = dat.getIP();
            }
            if (config.isDate()) {
                ob[posicion] = dat.getDate();
                posicion++;
            }
            if (config.isDisks()) {
                Long espacio_total = 0L;
                for (Disk disco : dat.getDisks()) {
                    espacio_total += disco.getSize();
                }
                ob[posicion] = Basics.formatLongtoGB(espacio_total) + "GB";
                posicion++;
                ob[posicion] = dat.getDisks().size();
                posicion++;
            }
            if (config.isPrograms()) {
                ob[posicion] = dat.getPrograms().size();
                posicion++;
            }
            if (config.isPartitions()) {
                ob[posicion] = dat.getPartitions().size();
                posicion++;
            }
            dtm.addRow(ob);
        }

        jTable1.setModel(dtm);
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(jTable1.getModel());
        jTable1.setRowSorter(sorter);
        int DiskC = -1, ProgramC = -1, PartitionC = -1;
        if (config.isDisks()) {
            for (int i = 0; i < jTable1.getColumnCount(); i++) {
                if (jTable1.getColumnName(i).equals("Discos")) {
                    DiskC = i;
                    break;
                }
            }
        }
        if (config.isPrograms()) {
            for (int i = 0; i < jTable1.getColumnCount(); i++) {
                if (jTable1.getColumnName(i).equals("Programas")) {
                    ProgramC = i;
                    break;
                }
            }

        }
        if (config.isPartitions()) {
            for (int i = 0; i < jTable1.getColumnCount(); i++) {
                if (jTable1.getColumnName(i).equals("Particiones")) {
                    PartitionC = i;
                    break;
                }
            }

        }
        if (ProgramC != -1 || DiskC != -1 || PartitionC != -1) {
            Action PA = new MyProgramAction(data,this);
            Action DA = new MyDiskAction(data,this);
            Action ParA = new MyPartitionAction(data,this);
            AEButtonColumns buttonColumn = new AEButtonColumns(jTable1, PA, DA, ParA, ProgramC, DiskC, PartitionC);
            buttonColumn.setMnemonic(KeyEvent.VK_D);
        }
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<>();
        for (int i = 0; i < jTable1.getColumnCount(); i++) {
            sortKeys.add(new RowSorter.SortKey(i, SortOrder.ASCENDING));
        }
        sorter.setSortKeys(sortKeys);
    }

    protected JTable getTable() {
        return jTable1;
    }

    protected void setTable(JTable table) {
        jTable1 = table;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Aula", "Equipo", "Procesador", "Nucleos", "Placa", "Video", "RAM"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jMenu1.setText("Archivo");

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/export_icon.png"))); // NOI18N
        jMenu2.setText("Exportar");

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/csv_icon.png"))); // NOI18N
        jMenuItem2.setText("csv");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/excel_icon.png"))); // NOI18N
        jMenuItem3.setText("Excel");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenu1.add(jMenu2);
        jMenu1.add(jSeparator1);

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/exit.png"))); // NOI18N
        jMenuItem1.setText("Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable1MouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        ExportConfig config = new ExportConfig();
        config.setTable(jTable1);
        try {
            CSV export = new CSV(config, this);
            if (export.export()) {
                JOptionPane.showMessageDialog(this, "Exportado con éxito", "Éxito", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Export.NullTableException ex) {
            Logger.getLogger(AE.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "No se ha podido guardar", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        ExportConfig config = new ExportConfig();
        config.setText(this.getTitle());
        config.setTable(jTable1);
        try {
            Excel export = new Excel(config, this);
            if (export.export()) {
                JOptionPane.showMessageDialog(this, "Exportado con éxito", "Éxito", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Export.NullTableException ex) {
            Logger.getLogger(AE.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "No se ha podido guardar", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

}
