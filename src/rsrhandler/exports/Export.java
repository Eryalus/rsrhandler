/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrhandler.exports;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ad_ri
 */
public abstract class Export {

    protected final JFrame ROOT;
    protected String SL = System.getProperty("line.separator");
    protected final Integer TYPE;

    public Export(JFrame root, Integer type) {
        this.ROOT = root;
        TYPE = type;
    }

    public boolean export() throws IOException {
        File dest = getDestFile(TYPE);
        if (dest != null) {
            export(dest);
            return true;
        } else {
            return false;
        }
    }

    public abstract void export(File dest) throws IOException;

    /**
     *
     * @param type 0 csv, 1 xsl
     * @return
     */
    protected File getDestFile(int type) {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilter filter = jfc.getFileFilter();
        if (type == 0) {
            filter = new FileNameExtensionFilter("CSV file", "csv");
        } else if (type == 1) {
            filter = new FileNameExtensionFilter("Excel file", "xlsx");
        }
        jfc.setFileFilter(filter);
        int showSaveDialog = jfc.showSaveDialog(ROOT);
        if (showSaveDialog == JFileChooser.APPROVE_OPTION) {
            File dest = jfc.getSelectedFile();
            if (type == 0 && !dest.getAbsolutePath().endsWith(".csv")) {
                dest = new File(dest.getAbsolutePath() + ".csv");
            } else if (type == 1 && !dest.getAbsolutePath().endsWith(".xlsx")) {
                dest = new File(dest.getAbsolutePath() + ".xlsx");
            }
            if (dest.exists()) {
                if (JOptionPane.showConfirmDialog(ROOT, "El fichero \"" + jfc.getSelectedFile().getName() + "\" ya existe. Desea sobreescribirlo?", "", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    return dest;
                }

            } else {
                return dest;
            }
        }
        return null;

    }

    public class NullTableException extends Exception {

    }
}
