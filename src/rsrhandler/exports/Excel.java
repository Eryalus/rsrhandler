/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrhandler.exports;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JFrame;
import javax.swing.JTable;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;

/**
 *
 * @author ad_ri
 */
public class Excel extends Export {
    
    private final ExportConfig CONFIG;
    
    public Excel(ExportConfig config, JFrame root) throws NullTableException {
        super(root, 1);
        if (config.getTable() == null) {
            throw new NullTableException();
        }
        CONFIG = config;
    }
    
    @Override
    public void export(File dest) throws IOException {
        String sheet_name = CONFIG.getText();
        XSSFWorkbook book = new XSSFWorkbook();
        XSSFSheet hoja;
        if (sheet_name != null) {
            sheet_name = WorkbookUtil.createSafeSheetName(sheet_name);
            hoja = book.createSheet(sheet_name);
        } else {
            hoja = book.createSheet("Hoja 1");
        }
        JTable table = CONFIG.getTable();
        int rows = table.getRowCount(), columns = table.getColumnCount();
        XSSFRow row_0 = hoja.createRow(0);
        
        for (int column = 0; column < columns; column++) {
            XSSFCell cell = row_0.createCell(column);
            cell.setCellValue(table.getColumnName(column));
            cell.setCellStyle(book.createCellStyle());
            XSSFCellStyle style = book.createCellStyle();
            style.setFillBackgroundColor(new XSSFColor(Color.LIGHT_GRAY));
            style.setFillForegroundColor(new XSSFColor(Color.LIGHT_GRAY));
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cell.setCellStyle(style);
            setBorder(cell, BorderStyle.MEDIUM);
        }
        for (int row = 0; row < rows; row++) {
            XSSFRow row_i = hoja.createRow(row + 1);
            for (int column = 0; column < columns; column++) {
                XSSFCell cell = row_i.createCell(column);
                cell.setCellStyle(book.createCellStyle());
                setBorder(cell, BorderStyle.THIN);
                cell.setCellValue(table.getValueAt(row, column).toString());
            }
        }
        
        OutputStream fileOut = new FileOutputStream(dest);
        book.write(fileOut);
        
    }
    
    private void setBorder(XSSFCell cell, BorderStyle bs) {
        XSSFCellStyle style = cell.getCellStyle();
        style.setBorderBottom(bs);
        style.setBorderLeft(bs);
        style.setBorderRight(bs);
        style.setBorderTop(bs);
        style.setBorderColor(XSSFCellBorder.BorderSide.TOP, new XSSFColor(Color.BLACK));
        style.setBorderColor(XSSFCellBorder.BorderSide.BOTTOM, new XSSFColor(Color.BLACK));
        style.setBorderColor(XSSFCellBorder.BorderSide.LEFT, new XSSFColor(Color.BLACK));
        style.setBorderColor(XSSFCellBorder.BorderSide.RIGHT, new XSSFColor(Color.BLACK));
        cell.setCellStyle(style);
    }
}
