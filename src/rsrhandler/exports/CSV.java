/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrhandler.exports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JTable;

/**
 *
 * @author ad_ri
 */
public class CSV extends Export {

    private final ExportConfig CONFIG;

    public CSV(ExportConfig config, JFrame root) throws NullTableException {
        super(root,0);
        if (config.getTable() == null) {
            throw new NullTableException();
        }
        CONFIG = config;
    }

    @Override
    public void export(File dest) throws IOException {
        JTable table = CONFIG.getTable();
        int rows = table.getRowCount(), columns = table.getColumnCount();
        BufferedWriter bw = new BufferedWriter(new FileWriter(dest));
        for (int column = 0; column < columns; column++) {
            if (column != columns - 1) {
                bw.write(table.getColumnName(column) + ",");
            } else {
                bw.write(table.getColumnName(column) + SL);
            }
        }
        for (int row = 0; row < rows; row++) {
            String txt = "";
            for (int column = 0; column < columns; column++) {
                if (column != columns - 1) {
                    txt += table.getValueAt(row, column).toString().trim() + ",";
                } else {
                    txt += table.getValueAt(row, column);
                }
            }
            if (txt.equals("")) {
                continue;
            }
            if (row != rows - 1) {
                bw.write(txt + SL);
            } else {
                bw.write(txt);
            }

        }
        bw.close();
    }

   

}
