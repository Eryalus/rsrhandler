/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data;

/**
 *
 * @author eryalus
 */
public class AEConfig {
    boolean disks,processor,cores,mother,video,ram,programs,mac,date,IP,partitions;

    public boolean isPartitions() {
        return partitions;
    }

    public void setPartitions(boolean partitions) {
        this.partitions = partitions;
    }

    public boolean isIP() {
        return IP;
    }

    public void setIP(boolean IP) {
        this.IP = IP;
    }

    public boolean isMac() {
        return mac;
    }

    public void setMac(boolean mac) {
        this.mac = mac;
    }

    public boolean isDate() {
        return date;
    }

    public void setDate(boolean date) {
        this.date = date;
    }

    public boolean isDisks() {
        return disks;
    }

    public void setDisks(boolean disks) {
        this.disks = disks;
    }

    public boolean isProcessor() {
        return processor;
    }

    public void setProcessor(boolean processor) {
        this.processor = processor;
    }

    public boolean isCores() {
        return cores;
    }

    public void setCores(boolean cores) {
        this.cores = cores;
    }

    public boolean isMother() {
        return mother;
    }

    public void setMother(boolean mother) {
        this.mother = mother;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public boolean isRam() {
        return ram;
    }

    public void setRam(boolean ram) {
        this.ram = ram;
    }

    public boolean isPrograms() {
        return programs;
    }

    public void setPrograms(boolean programs) {
        this.programs = programs;
    }
}
