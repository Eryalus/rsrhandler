/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data.beans;

/**
 *
 * @author eryalus
 */
public class Program {
    private String name,version,description,vendor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Program(String name, String version, String description, String vendor) {
        this.name = name;
        this.version = version;
        this.description = description;
        this.vendor = vendor;
    }

    public Program() {
    }
}
