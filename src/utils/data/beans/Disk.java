/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data.beans;

/**
 *
 * @author eryalus
 */
public class Disk {
    private Long size;

    public Disk(Long size, String name) {
        this.size = size;
        this.name = name;
    }

    public Disk() {
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private String name;
}
