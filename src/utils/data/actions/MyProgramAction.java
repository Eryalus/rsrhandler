/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import rsrhandler.AE;
import rsrhandler.AEProgram;
import utils.data.AEData;

/**
 *
 * @author eryalus
 */
public class MyProgramAction extends AbstractAction {
    
    private ArrayList<AEData> data;
    protected AE PARENT;
    
    public MyProgramAction(ArrayList<AEData> data, AE parent) {
        this.data = data;
        PARENT = parent;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JTable table = (JTable) e.getSource();
        int modelRow = Integer.valueOf(e.getActionCommand());
        DefaultTableModel dtm = ((DefaultTableModel) table.getModel());
        AEData dat = data.get(modelRow);
        
        AEProgram vent = new AEProgram(dat.getPrograms());
        vent.setLocationRelativeTo(null);
        vent.setTitle(dat.getAula() + " " + dat.getEquipo()+" - Programas");
        vent.setIconImage(PARENT.getIconImage());
        vent.setVisible(true);
        
    }
}
