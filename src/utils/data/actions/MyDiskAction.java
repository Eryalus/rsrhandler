/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import rsrhandler.AE;
import rsrhandler.AEDisk;
import utils.data.AEData;

/**
 *
 * @author eryalus
 */
public class MyDiskAction extends AbstractAction {

    private ArrayList<AEData> data;
    protected AE PARENT;

    public MyDiskAction(ArrayList<AEData> data, AE parent) {
        this.data = data;
        PARENT = parent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int modelRow = Integer.valueOf(e.getActionCommand());
        AEData dat = data.get(modelRow);
        AEDisk vent = new AEDisk(dat.getDisks());
        vent.setLocationRelativeTo(null);
        vent.setTitle(dat.getAula() + " " + dat.getEquipo()+" - Discos");
        vent.setIconImage(PARENT.getIconImage());
        vent.setVisible(true);
    }
}
