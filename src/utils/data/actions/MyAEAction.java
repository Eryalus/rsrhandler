/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import rsrhandler.AE;
import rsrhandler.AEDisk;
import rsrhandler.AEProgram;
import utils.data.AEData;

/**
 *
 * @author eryalus
 */
public class MyAEAction extends AbstractAction {

    private ArrayList<AEData> data;

    public MyAEAction(ArrayList<AEData> data) {
        this.data = data;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        int modelRow = Integer.valueOf(e.getActionCommand());
        AEData dat = data.get(modelRow);
        AEDisk vent = new AEDisk(dat.getDisks());
        vent.setLocationRelativeTo(null);
        vent.setTitle(dat.getAula() + " " + dat.getEquipo());
        vent.setVisible(true);

    }
}
