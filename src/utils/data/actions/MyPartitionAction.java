/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import rsrhandler.AE;
import rsrhandler.AEPartition;
import utils.data.AEData;

/**
 *
 * @author eryalus
 */
public class MyPartitionAction extends AbstractAction {
    
    private ArrayList<AEData> data;
    protected AE PARENT;
    
    public MyPartitionAction(ArrayList<AEData> data,AE parent) {
        this.data = data;
        PARENT = parent;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        int modelRow = Integer.valueOf(e.getActionCommand());
        AEData dat = data.get(modelRow);        
        AEPartition vent = new AEPartition(dat.getPartitions());
        vent.setLocationRelativeTo(null);
        vent.setTitle(dat.getAula() + " " + dat.getEquipo()+" - Particiones");
        vent.setIconImage(PARENT.getIconImage());
        vent.setVisible(true);        
    }
}
