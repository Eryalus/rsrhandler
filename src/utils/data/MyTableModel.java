/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author eryalus
 */
public class MyTableModel extends DefaultTableModel {

    private final String[] names;

    public MyTableModel(String[] names) {
        this.names = names;
        for(String name : names){
            super.addColumn(name);
        }
    }

    @Override
    public String getColumnName(int index) {
        return names[index];
    }
    @Override
    public boolean isCellEditable(int row,int column){
        if(super.getColumnName(column).equals("Programas")){
            return true;
        }else if(super.getColumnName(column).equals("Discos")){
            return true;
        }else if(super.getColumnName(column).equals("Particiones")){
            return true;
        }
        return false;
        
    }
}
