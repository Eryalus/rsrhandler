/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data;

import java.text.DecimalFormat;

/**
 *
 * @author eryalus
 */
public class Basics {
    public static String formatLongtoGB(Long dato){
        DecimalFormat df = new DecimalFormat("##.00");
        return df.format((double) dato / 1073741824);
    }
}
