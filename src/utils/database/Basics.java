/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.data.AEConfig;
import utils.data.AEData;
import utils.data.beans.Disk;
import utils.data.beans.Partition;
import utils.data.beans.Program;

/**
 * Utilidades básicas de la base de datos.
 *
 * @author ad_ri
 */
public class Basics {

    private static final String NOMBRE_DB = "RSRData";
    private static final String IP = "localhost";

    /**
     * Trata de crear la base de datos en caso de no estar creada.
     *
     * @param usuario Usuario de la base de datos
     * @param pass Contraseña de a base de datos
     * @param port Puerto de acceso a la base de datos
     * @return true en caso de terminar la ejecución dejando la base de datos
     * creada o false en caso de haber algún error
     */
    public static boolean crearBaseDatos(String usuario, String pass, String port) {
        try {
            Connection conn = MySQLConnection(usuario, pass, port, "");
            importSQL(conn, new FileInputStream("crear_db.sql"));
        } catch (FileNotFoundException | ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    public static void importSQL(Connection conn, InputStream in) throws SQLException {
        Scanner s = new Scanner(in);
        s.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement st = null;
        try {
            st = conn.createStatement();
            while (s.hasNext()) {
                String line = s.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    st.execute(line);
                }
            }
        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public static void delEquipo(String aula, String numero, Connection con) throws SQLException {
        Integer id = getIDEquipo(aula, numero, con);

        if (id != -1) {
            con.createStatement().execute("DELETE FROM `pc_has_program` WHERE `pc_has_program`.`idpc` = " + id);
            con.createStatement().execute("DELETE FROM `pcs_has_disks` WHERE `pcs_has_disks`.`idpc`=" + id);
            con.createStatement().execute("DELETE FROM `partition` where `partition`.`idpc`=" + id);
            con.createStatement().execute("DELETE FROM `pcs` WHERE `pcs`.`idpc`=" + id);
        }

    }

    public static ArrayList<AEData> getAE(String aula, String equipo, Connection con) throws SQLException {
        ResultSet result;
        if (aula.equals("")) {
            if (equipo.equals("")) {
                result = con.createStatement().executeQuery("SELECT * FROM pcs");
            } else {
                result = con.createStatement().executeQuery("SELECT * FROM pcs WHERE number='" + equipo + "'");
            }
        } else {
            if (equipo.equals("")) {
                result = con.createStatement().executeQuery("SELECT * FROM pcs WHERE classroom='" + aula.toUpperCase() + "'");
            } else {
                result = con.createStatement().executeQuery("SELECT * FROM pcs WHERE classroom='" + aula.toUpperCase() + "' and number='" + equipo + "'");
            }

        }
        ArrayList<AEData> list = new ArrayList<>();

        while (result.next()) {
            AEData data = new AEData();
            data.setAula(result.getString("classroom"));
            data.setEquipo("" + result.getInt("number"));
            data.setProcesador(result.getString("processor"));
            data.setCores(result.getInt("processor_cores"));
            data.setMother(result.getString("motherboard"));
            data.setVideo(result.getString("videocard"));
            data.setMAC(result.getString("mac"));
            data.setDate(result.getDate("date"));
            data.setIP(result.getString("ip"));
            data.setRam("" + utils.data.Basics.formatLongtoGB(result.getLong("ramsize")) + " GB");
            Integer idpc = result.getInt("idpc");
            ResultSet result2 = con.createStatement().executeQuery("SELECT idprogram FROM pc_has_program WHERE idpc=" + idpc);
            ArrayList<Program> programas = new ArrayList<>();
            while (result2.next()) {
                Long idprogram = result2.getLong("idprogram");
                ResultSet result3 = con.createStatement().executeQuery("SELECT * FROM program WHERE idprogram=" + idprogram);
                while (result3.next()) {
                    Program programa = new Program();
                    programa.setName(result3.getString("name"));
                    programa.setDescription(result3.getString("description"));
                    programa.setVendor(result3.getString("vendor"));
                    programa.setVersion(result3.getString("version"));
                    programas.add(programa);
                }
            }
            data.setPrograms(programas);
            ArrayList<Disk> discos = new ArrayList<>();
            result2 = con.createStatement().executeQuery("SELECT iddisk FROM pcs_has_disks WHERE idpc=" + idpc);
            while (result2.next()) {
                Integer iddisk = result2.getInt("iddisk");
                ResultSet result3 = con.createStatement().executeQuery("SELECT * FROM disks WHERE iddisk=" + iddisk);
                while (result3.next()) {
                    Disk disco = new Disk();
                    disco.setName(result3.getString("name"));
                    disco.setSize(result3.getLong("capacity"));
                    discos.add(disco);
                }
            }
            data.setDisks(discos);
            data.setPartitions(getPartitions(idpc, con));
            list.add(data);
        }
        return list;
    }

    public static ArrayList<AEData> getAE(String aula, String equipo, AEConfig config, Connection con) throws SQLException {
        ResultSet result;
        if (aula.equals("")) {
            if (equipo.equals("")) {
                result = con.createStatement().executeQuery("SELECT * FROM pcs");
            } else {
                result = con.createStatement().executeQuery("SELECT * FROM pcs WHERE number='" + equipo + "'");
            }
        } else {
            if (equipo.equals("")) {
                result = con.createStatement().executeQuery("SELECT * FROM pcs WHERE classroom='" + aula.toUpperCase() + "'");
            } else {
                result = con.createStatement().executeQuery("SELECT * FROM pcs WHERE classroom='" + aula.toUpperCase() + "' and number='" + equipo + "'");
            }

        }
        ArrayList<AEData> list = new ArrayList<>();

        while (result.next()) {
            AEData data = new AEData();
            data.setAula(result.getString("classroom"));
            data.setEquipo("" + result.getInt("number"));
            data.setProcesador(result.getString("processor"));
            data.setCores(result.getInt("processor_cores"));
            data.setMother(result.getString("motherboard"));
            data.setVideo(result.getString("videocard"));
            data.setMAC(result.getString("mac"));
            data.setDate(result.getDate("date"));
            data.setIP(result.getString("ip"));
            data.setRam("" + utils.data.Basics.formatLongtoGB(result.getLong("ramsize")) + " GB");
            Integer idpc = result.getInt("idpc");
            ArrayList<Program> programas = new ArrayList<>();
            if (config.isPrograms()) {
                ResultSet result2 = con.createStatement().executeQuery("SELECT idprogram FROM pc_has_program WHERE idpc=" + idpc);
                while (result2.next()) {
                    Long idprogram = result2.getLong("idprogram");
                    ResultSet result3 = con.createStatement().executeQuery("SELECT * FROM program WHERE idprogram=" + idprogram);
                    while (result3.next()) {
                        Program programa = new Program();
                        programa.setName(result3.getString("name"));
                        programa.setDescription(result3.getString("description"));
                        programa.setVendor(result3.getString("vendor"));
                        programa.setVersion(result3.getString("version"));
                        programas.add(programa);
                    }
                }
            }
            data.setPrograms(programas);
            ArrayList<Disk> discos = new ArrayList<>();
            if (config.isDisks()) {
                ResultSet result2 = con.createStatement().executeQuery("SELECT iddisk FROM pcs_has_disks WHERE idpc=" + idpc);
                while (result2.next()) {
                    Integer iddisk = result2.getInt("iddisk");
                    ResultSet result3 = con.createStatement().executeQuery("SELECT * FROM disks WHERE iddisk=" + iddisk);
                    while (result3.next()) {
                        Disk disco = new Disk();
                        disco.setName(result3.getString("name"));
                        disco.setSize(result3.getLong("capacity"));
                        discos.add(disco);
                    }
                }
            }
            data.setDisks(discos);
            ArrayList<Partition> particiones = new ArrayList<>();
            if(config.isPartitions()){
                particiones=getPartitions(idpc, con);
            }
            data.setPartitions(particiones);
            list.add(data);
        }
        return list;
    }

    public static Integer getIDEquipo(String aula, String numero, Connection con) {
        try {
            ResultSet result = con.createStatement().executeQuery("SELECT idpc FROM pcs WHERE classroom='" + aula.toUpperCase() + "' and number='" + numero + "'");
            if (result.next()) {
                return result.getInt("idpc");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public static void addProgram(String aula, String numero, String nombre, String version, String descripcion, String vendor, Connection con) throws SQLException {
        Integer idprogram = getIDProgram(nombre, version, descripcion, vendor, con);
        Integer id = getIDEquipo(aula, numero, con);
        if (idprogram == -1) {
            if (id != -1) {
                con.createStatement().execute("INSERT IGNORE INTO `program` (`idprogram`, `name`, `version`, `description`, `vendor`) VALUES (NULL, '" + nombre + "', '" + version + "','" + descripcion + "', '" + vendor + "')");
                idprogram = getIDProgram(nombre, version, descripcion, vendor, con);
            }
        }
        if (idprogram != -1 && id != -1) {
            con.createStatement().execute("INSERT IGNORE INTO `pc_has_program` (`idpc`, `idprogram`) VALUES ('" + id + "', '" + idprogram + "')");
        }
    }

    public static boolean addPartition(String aula, String numero, String name, String total, String free, Connection con) throws SQLException {
        Integer id = getIDEquipo(aula, numero, con);
        if (id != -1) {
            con.createStatement().execute("INSERT IGNORE INTO `partition` (`name`,`total`,`free`,`idpc`) VALUES ('" + name + "','" + total + "','" + free + "','" + id + "')");
            return true;
        }
        return false;
    }

    public static Integer getIDProgram(String nombre, String version, String descripcion, String vendor, Connection con) {
        try {
            ResultSet result = con.createStatement().executeQuery("SELECT idprogram FROM program WHERE name='" + nombre + "' and vendor='" + vendor + "' and description='" + descripcion + "' and version='" + version + "'");
            if (result.next()) {
                return result.getInt("idprogram");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public static void addEquipo(String aula, String numero, ArrayList<Integer> disks, String processor, Integer processor_cores, String motherboard, String videocard, Long ramsize, Connection con) throws SQLException {
        con.createStatement().execute("INSERT INTO `pcs` (`idpc`,`number`,`classroom`, `processor`, `processor_cores`, `motherboard`, `videocard`, `ramsize`) VALUES (NULL,'" + numero + "', '" + aula + "','" + processor + "', '" + processor_cores
                + "', '" + motherboard + "', '" + videocard + "', '" + ramsize + "')");
        Integer idpc;
        if ((idpc = getIDEquipo(aula, numero, con)) != -1) {
            for (Integer iddisk : disks) {
                con.createStatement().execute("INSERT INTO `pcs_has_disks` (`idpc`, `iddisk`) VALUES ('" + idpc + "', '" + iddisk + "')");
            }
        }
    }

    public static void addDisk(String name, Long capacity, Connection con) throws SQLException {
        con.createStatement().execute("INSERT INTO `disks` (`iddisk`, `name`, `capacity`) VALUES (NULL, '" + name + "', '" + capacity + "')");
    }

    public static Integer getIDDisk(String name, Long capacity, Connection con) {
        try {
            ResultSet result = con.createStatement().executeQuery("SELECT iddisk FROM disks WHERE name='" + name + "' and capacity='" + capacity + "'");
            if (result.next()) {
                return result.getInt("iddisk");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public static ArrayList<Partition> getPartitions(Integer idpc, Connection con) {
        ArrayList<Partition> al = new ArrayList<>();
        try {
            ResultSet result = con.createStatement().executeQuery("SELECT * FROM `partition` WHERE idpc=" + idpc + "");
            while (result.next()) {
                Partition p = new Partition();
                p.setName(result.getString("name"));
                p.setFree(result.getLong("free"));
                p.setTotal(result.getLong("total"));
                al.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return al;
    }

    /**
     * Crea una conexión con la base de datos a nivel de la DB del programa
     *
     * @return Conexión con la base de datos ex1.printStackTrace();
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Connection conectarBaseDatos(String usuario, String pass, String port) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        return MySQLConnection(usuario, pass, port, NOMBRE_DB);
    }

    /**
     * Crea una conexión con la base de datos dada.
     *
     * @param user usuario
     * @param pass contraseña
     * @param db_name nombre de la base de datos a conectar
     * @return Conexión
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private static Connection MySQLConnection(String user, String pass, String port, String db_name) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection conn = null;
        if (port.equals("")) {
            conn = DriverManager.getConnection("jdbc:mysql://" + IP + "/" + db_name + "?connectTimeout=15000", user, pass);
        } else {
            conn = DriverManager.getConnection("jdbc:mysql://" + IP + ":" + port + "/" + db_name + "?connectTimeout=15000", user, pass);
        }
        return conn;
    }
}
