/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.images;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author eryalus
 */
public class Images {

    public URL getLogo() {
        return this.getClass().getResource("logo.png");
    }

    public Image getLogoAsImage() {
        return new ImageIcon(getLogo()).getImage();
    }

    public ImageIcon getLogoAsIcon() {
        return new ImageIcon(getLogo());
    }
}
